import os
import pytest
import unittest

from xdg import XDG_CONFIG_HOME

from aloha68_config.exceptions import ExtensionNotSupportedError
from aloha68_config.locator import ConfigLocator, ConfigPath


def _create_file(filename: str) -> None:
    with open(filename, "w") as f:
        f.write(" \n")


def _delete_file(filename: str) -> None:
    if os.path.exists(filename):
        os.remove(filename)


def _get_unknown_filename(filename: str) -> str:
    while os.path.exists(filename):
        filename += "0"
    return filename


class TestConfigLocator(unittest.TestCase):
    tested_absolute_path: str = "/tmp/test-config-locator.conf"
    tested_unknown_absolute_path: str = "/tmp/blblblbl"
    tested_relative_path: str = ".test-config-locator.conf"
    tested_unknown_relative_path: str = ".blblblbl"
    tested_xdg_relative_path: str = "blblblbl.conf"
    xdg_absolute_path: str = os.path.join(XDG_CONFIG_HOME, tested_xdg_relative_path)

    def setUp(self) -> None:

        # On crée les différents fichiers
        _create_file(self.tested_absolute_path)
        _create_file(self.tested_relative_path)
        _create_file(self.xdg_absolute_path)

        # On s'assure que les fichiers inconnus n'existent pas
        self.tested_unknown_absolute_path = _get_unknown_filename(self.tested_unknown_absolute_path)
        self.tested_unknown_relative_path = _get_unknown_filename(self.tested_unknown_relative_path)

    def tearDown(self) -> None:
        # On supprime les différents fichiers
        _delete_file(self.tested_absolute_path)
        _delete_file(self.tested_relative_path)
        _delete_file(self.xdg_absolute_path)

    def test_creation(self):
        """ Teste la création d'un ConfigLocator """

        cl = ConfigLocator("")
        self.assertEqual(len(cl.searched_paths), 1)
        self.assertEqual(cl.searched_paths[0], XDG_CONFIG_HOME)

    def test_creation_without_xdg(self):
        """ Teste la création d'un ConfigLocator sans XDG """

        cl = ConfigLocator("", allow_xdg=False)
        self.assertEqual(len(cl.searched_paths), 0)

    def test_locate_absolute_path(self):
        """ Teste la récupération d'un fichier absolu existant """

        cl = ConfigLocator(self.tested_absolute_path)
        path = cl.locate()
        self.assertIsInstance(path, ConfigPath)

    def test_locate_unknown_absolute_path(self):
        """ Teste le plantage en cas de récupération d'un fichier absolu inexistant """

        cl = ConfigLocator(self.tested_unknown_absolute_path)
        with pytest.raises(FileNotFoundError):
            cl.locate()

    def test_locate_relative_path(self):
        """ Teste la récupération d'un fichier relatif existant """

        cl = ConfigLocator(self.tested_relative_path)
        path = cl.locate()
        self.assertIsInstance(path, ConfigPath)

    def test_locate_unknown_relative_path(self):
        """ Teste le plantage en cas de récupération d'un fichier relatif inexistant """

        cl = ConfigLocator(self.tested_unknown_relative_path)
        with pytest.raises(FileNotFoundError):
            cl.locate()

    def test_locate_relative_xdg_path(self):
        """ Teste les différents endroits où peut se trouver un fichier relatif """

        cl = ConfigLocator(self.tested_xdg_relative_path)
        path = cl.locate()
        self.assertIsInstance(path, ConfigPath)

    def test_default_extension(self):
        """ Teste l'ajout d'extensions """

        cl = ConfigLocator("")
        self.assertEqual(len(cl.supported_extensions), 1)
        self.assertEqual(cl.supported_extensions[0], ".conf")

    def test_add_extension(self):
        """ Teste l'ajout d'une extension """

        cl = ConfigLocator("")
        nb_extensions = len(cl.supported_extensions)

        cl.add_supported_extension(".yaml")
        self.assertTrue(".yaml" in cl.supported_extensions)
        self.assertEqual(len(cl.supported_extensions), nb_extensions + 1)

    def test_add_existing_extension(self):
        """ Teste l'ajout d'une extension déjà existante """

        cl = ConfigLocator("")
        nb_extensions = len(cl.supported_extensions)

        cl.add_supported_extension(".yaml")
        cl.add_supported_extension(".yaml")
        self.assertEqual(len(cl.supported_extensions), nb_extensions + 1)

    def test_add_extension_without_dot(self):
        """ Teste l'ajout d'une extension sans point """

        cl = ConfigLocator("")
        nb_extensions = len(cl.supported_extensions)

        cl.add_supported_extension("toml")
        self.assertTrue(".toml" in cl.supported_extensions)
        self.assertEqual(len(cl.supported_extensions), nb_extensions + 1)

    def test_unsupported_extension(self):
        """ Teste le cas où une mauvaise extension est renseignée """

        cl = ConfigLocator("test.prout")
        with pytest.raises(ExtensionNotSupportedError):
            cl.locate()
