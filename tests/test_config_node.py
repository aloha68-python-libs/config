import pytest
import unittest
from typing import List, Any

from aloha68_config import ConfigNode


class TestConfigNode(unittest.TestCase):
    """ Tests sur la classe ConfigNode """

    wrong_data_type: List[Any] = [
        0,
        1,
        True,
        "",
        "null",
        (),
        (1, 2),
        [],
        [1, 2],
        Exception(),
    ]

    def test_create_config_node(self):
        """ Teste la création d'un ConfigNode pré-rempli """

        cn = ConfigNode({"test": "value"})
        self.assertIsInstance(cn, ConfigNode)

        self.assertEqual(cn.test, "value")
        self.assertEqual(cn["test"], "value")

    def test_create_wrong_config_node(self):
        """ Teste la création de ConfigNode """

        for wdt in self.wrong_data_type:
            with pytest.raises(AssertionError):
                print(wdt)
                ConfigNode(wdt)

    def test_create_empty_config_node(self):
        """ Teste la création d'un ConfigNode vide """

        cn = ConfigNode()
        self.assertIsInstance(cn, ConfigNode)

    def test_set_value(self):
        """ Teste l'ajout de valeur à un ConfigNode """

        cn = ConfigNode()

        setattr(cn, "test", "value")
        self.assertEqual(cn.test, "value")
        self.assertEqual(cn["test"], "value")

        cn.test2 = "value"
        self.assertEqual(cn.test2, "value")
        self.assertEqual(cn["test2"], "value")

    def test_unknown_key(self):
        """ Teste le déclenchement d'erreur en cas de clé inconnue """

        cn = ConfigNode()
        with pytest.raises(AttributeError):
            return cn.test

    def test_default_value_doesnt_erase_value(self):
        """ Teste que la récupération d'une valeur n'écrase pas la vraie valeur """

        cn = ConfigNode({"test": "value"})
        self.assertEqual(cn.get("test", "test"), "value")

    def test_default_value_for_unknown_key(self):
        """ Teste la récupération d'une valeur par défaut pour une clé inexistante """

        cn = ConfigNode()
        self.assertIsNone(cn.get("test"))
        self.assertEqual(cn.get("test", "test"), "test")
        self.assertEqual(cn.get("test", 1), 1)
        self.assertEqual(cn.get("test", []), list())
        self.assertIsInstance(cn.get("test", ConfigNode()), ConfigNode)

    def test_remove_data(self):
        """ Teste la suppression d'une valeur """

        cn = ConfigNode({"test": "value"})
        del cn.test
        with pytest.raises(AttributeError):
            print(cn.test)

    def test_contains(self):
        """ Teste l'opérateur in """

        cn = ConfigNode({"test": "value"})
        self.assertTrue("test" in cn)

    def test_print(self):
        """ Teste l'affichage d'un noeud """

        cn = ConfigNode({"test": "value"})
        self.assertEqual(str(cn), "ConfigNode(test='value')")