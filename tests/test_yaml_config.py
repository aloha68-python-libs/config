import aloha68_crypto_service
import keyring
import os
import textwrap
import unittest

from aloha68_config import ConfigNode, YamlConfig


class TestYamlConfig(unittest.TestCase):
    """ Tests pour la classe YamlConfig """

    tested_filename: str = "/tmp/test-config.yaml"
    tested_included_file: str = "/tmp/test-config-include.yaml"

    tested_keyring_service_name: str = "service_name"
    tested_keyring_username: str = "username"
    tested_keyring_password: str = "password"
    tested_decrypted_value: str = "coucou c'est moi"

    def setUp(self) -> None:

        # Prépartion du mot de passe keyring
        keyring.set_password(
            self.tested_keyring_service_name,
            self.tested_keyring_username,
            self.tested_keyring_password
        )

        # Écriture d'un fichier de conf
        with open(self.tested_filename, "w") as f:
            f.write(textwrap.dedent("""
                value1: test
                value2: coucou
                tested_list:
                    - 1
                    - 2
                    - 3
                tested_dict:
                    value1: prout
                #commented_value: 100
                sub: !include {}
            """.format(self.tested_included_file)))

            f.write("encrypted_value: !decrypt {}".format(
                aloha68_crypto_service.encrypt_message(
                    message=self.tested_decrypted_value,
                    password=self.tested_keyring_password
                )
            ))

        # Écriture d'un fichier de sous-conf
        with open(self.tested_included_file, "w") as f:
            f.write("included_value: 500\n")

    def tearDown(self) -> None:
        if os.path.isfile(self.tested_filename):
            os.remove(self.tested_filename)

        if os.path.isfile(self.tested_included_file):
            os.remove(self.tested_included_file)

        keyring.delete_password(self.tested_keyring_service_name, self.tested_keyring_username)

    def test_basic_config(self):
        """ Teste d'un fichier de configuration basique """

        conf = YamlConfig(self.tested_filename)
        self.assertEqual(conf.value1, "test")
        self.assertEqual(conf.value2, "coucou")
        self.assertIsInstance(conf.tested_list, list)
        for i in range(1, 4):
            self.assertIn(i, conf.tested_list)
        self.assertIsInstance(conf.tested_dict, ConfigNode)
        self.assertEqual(conf.tested_dict.value1, "prout")
        self.assertIsNone(conf.get("commented_value"))

    def test_include_file(self):
        """ Teste l'inclusion de fichier annexe """

        conf = YamlConfig(self.tested_filename)
        self.assertIsInstance(conf.sub, ConfigNode)
        self.assertEqual(conf.sub.included_value, 500)

    def test_decrypt_from_keyring(self):
        """ Teste le déchiffrement de valeurs grâce à keyring """

        conf = YamlConfig(
            self.tested_filename,
            keyring_service_name=self.tested_keyring_service_name,
            keyring_username=self.tested_keyring_username
        )

        self.assertEqual(conf.encrypted_value, self.tested_decrypted_value)