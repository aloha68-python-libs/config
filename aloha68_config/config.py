"""
Module principal du package, permet la récupération d'un fichier de configuration.
Un fichier de configuration est typé comme étant un ConfigNode, une sorte de dictionnaire.
"""
from pprint import pformat
from types import SimpleNamespace
from typing import Any, Optional, Dict

from aloha68_config.locator import ConfigLocator, ConfigPath


class ConfigNode(SimpleNamespace):
    """ Espace de nom imbriqué, permet l'utilisation d'un dictionnaire comme un objet python """

    def __init__(self, data: Optional[Dict[str, Any]] = None, **kwargs):
        super().__init__(**kwargs)
        assert data is None or isinstance(data, dict)
        self._import_data(data)

    def _import_data(self, data: Optional[Dict[str, Any]]) -> None:
        """ Importe une liste de données et l'intègre au namespace courant """

        if not data:
            return

        for key, value in data.items():
            if isinstance(value, dict):
                self.__setattr__(key, ConfigNode(value))
            else:
                self.__setattr__(key, value)

    def __contains__(self, item) -> bool:
        """ Retourne vrai si l'item est contenu dans le namespace courant """
        return hasattr(self, item)

    def __getitem__(self, item) -> Any:
        """ Retourne l'item dans le cas d'une utilisation du ConfigNode en tant que dictionnaire """
        return getattr(self, item)

    def get(self, item: str, default: Optional[Any] = None) -> Any:
        """ Retourne un item de l'objet courant ou la valeur par défaut """
        try:
            return self[item]
        except AttributeError:
            return default

    def __str__(self) -> str:
        return pformat(self)


class Config(ConfigNode):
    """ Classe abstraite à réimplémenter pour chaque implémentation """

    def __init__(self, filename: str, locator: Optional[ConfigLocator] = None):
        super().__init__()

        if not locator:
            locator = ConfigLocator(filename)

        path = locator.locate()
        self.__import(path)

    def __import(self, path: ConfigPath):
        """ Méthode interne pour lire le fichier de configuration """

        self._before_import()
        data = self._get_data(path)
        self._import_data(data)
        self._after_import()

    def _before_import(self):
        """ Méthode appelée avant l'import """

    def _get_data(self, path: ConfigPath) -> Dict[str, Any]:
        """ Méthode de récupération des données du fichiers de configuration """
        raise NotImplementedError(self.__class__.__name__ + "._get_data()")

    def _after_import(self):
        """ Méthode appelée après l'import """
