"""
Module permettant le chargement d'un fichier YAML
"""
import getpass
import logging
import os
import yaml

import aloha68_crypto_service

logger = logging.getLogger("config")


class YamlLoader(yaml.SafeLoader):  # pylint: disable=too-many-ancestors
    """ Loader YAML custom pour permettre l'utilisation de tags spécifiques """

    decrypt_password: str = None
    interactive_decryption: bool = False

    def __init__(self, stream):
        self._root: str = os.path.split(stream.name)[0]
        super().__init__(stream)

    def include(self, node):
        """ Inclut un fichier de configuration marqué avec !include """

        filename = os.path.join(self._root, self.construct_scalar(node))
        logger.debug(f"Lecture du fichier: {filename}")
        with open(filename, 'r', encoding="utf-8") as sub_config:
            return yaml.load(sub_config, YamlLoader)

    def decrypt(self, node):
        """ Déchiffre une valeur de configuration marquée avec !crypt """

        value = self.construct_scalar(node)

        # Si aucun mot de passe n'est fourni
        # et qu'on ne demande pas interactivement le mot de passe,
        # on retourne la valeur sans la déchiffrer
        if not self.decrypt_password and not self.interactive_decryption:
            return value

        # En mode interactif, on demande la saisie du mot de passe
        if not self.decrypt_password and self.interactive_decryption:
            self.decrypt_password = getpass.getpass("Mot de passe : ")

        return aloha68_crypto_service.decrypt_message(value, self.decrypt_password)
