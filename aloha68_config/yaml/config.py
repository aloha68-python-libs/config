"""
Permet la gestion d'un fichier de configuration au format YAML
"""

import logging
from typing import Any, Dict

import keyring
import yaml

from aloha68_config.config import Config
from aloha68_config.locator import ConfigLocator, ConfigPath
from aloha68_config.yaml.loader import YamlLoader

logger = logging.getLogger("config")


class YamlConfig(Config):
    """ Représente un fichier de configuration au format YAML """

    __data = dict()

    def __init__(
            self,
            filename: str,
            keyring_service_name: str = None,
            keyring_username: str = None,
            interactive_decryption: bool = False,
    ):
        locator = ConfigLocator(filename)
        locator.add_supported_extension("yml")
        locator.add_supported_extension("yaml")

        self.__keyring_service_name = keyring_service_name
        self.__keyring_username = keyring_username
        self.__interactive_decryption = interactive_decryption

        super().__init__(filename, locator=locator)

    def _before_import(self):
        """ Méthode appelée avant l'import """

        # Permet l'utilisation du tag !include
        YamlLoader.add_constructor("!include", YamlLoader.include)

        # Permet l'utilisation du tag !decrypt
        YamlLoader.add_constructor("!decrypt", YamlLoader.decrypt)
        YamlLoader.interactive_decryption = self.__interactive_decryption

        if self.__keyring_username and self.__keyring_service_name:
            print("Keyring method: {}".format(keyring.get_keyring()))
            print("Keyring: User:", self.__keyring_username, ", Service:", self.__keyring_service_name)
            password = keyring.get_password(self.__keyring_service_name, self.__keyring_username)
            print("Mot de passe utilisé: {}".format(password))
            YamlLoader.decrypt_password = password


    def _get_data(self, path: ConfigPath) -> Dict[str, Any]:
        """ Méthode de récupération des données du fichiers de configuration """

        logger.debug(f"Lecture du fichier: {path}")
        with open(path, 'r', encoding="utf-8") as conf_file:
            return yaml.load(conf_file, YamlLoader)
