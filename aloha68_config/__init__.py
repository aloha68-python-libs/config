"""
Package permettant de gérer un fichier de configuration.
On propose les classes Config et YamlConfig, ainsi que ConfigNode pour du typing.
"""
__version__ = '0.3.0'
__all__ = ["Config", "ConfigNode", "YamlConfig"]

from aloha68_config.config import Config, ConfigNode
from aloha68_config.yaml.config import YamlConfig
