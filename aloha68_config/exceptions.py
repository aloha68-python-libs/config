"""
Exceptions liées au package
"""
from pathlib import Path


class ConfigError(Exception):
    """ Exception de base """


class ExtensionNotSupportedError(ConfigError):
    """ Exception déclenchée lors qu'une extension n'est pas supportée """

    def __init__(self, path: Path):
        super().__init__(f"Extension non supportée: {path.suffix}")
