"""
Localisateur de fichier de configuration
"""

from pathlib import PosixPath as Path
from typing import List, Optional
from xdg import XDG_CONFIG_HOME

from aloha68_config.exceptions import ExtensionNotSupportedError


class ConfigPath(Path):
    """ Fichier de configuration """


class ConfigLocator:
    """ Localisateur de fichier de configuration """

    def __init__(self, filename: str, allow_xdg: bool = True, application: Optional[str] = None):
        self.filename = filename

        self.searched_paths: List[Path] = []
        self.tested_paths: List[ConfigPath] = []
        self.supported_extensions: List[str] = [".conf"]

        if allow_xdg:
            self.searched_paths.append(Path(XDG_CONFIG_HOME))

            if application:
                self.searched_paths.append(Path(XDG_CONFIG_HOME, application))

    def add_supported_extension(self, ext: str) -> None:
        """ Ajoute l'extension passée en paramètre à la liste des extensions supportées """

        if not ext.startswith("."):
            ext = "." + ext

        if ext not in self.supported_extensions:
            self.supported_extensions.append(ext)

    def _get_path_or_exception(self, path: ConfigPath) -> ConfigPath:
        """ Vérifie que le chemin passé en paramètre existe ou déclenche une exception """

        if path.is_file():
            return path

        self.tested_paths.append(path)
        raise FileNotFoundError(f"Pas de fichier de configuration: {path}")

    def _locate_relative_path(self, path: ConfigPath) -> Optional[ConfigPath]:
        """ Localise un fichier de configuration relatif """

        # Recherche par rapport au répertoire courant
        try:
            return self._get_path_or_exception(path)
        except FileNotFoundError:
            pass

        # Recherche dans les différents dossiers configurés
        for search_path in self.searched_paths:
            tmp_path = ConfigPath(search_path, path)
            try:
                return self._get_path_or_exception(tmp_path)
            except FileNotFoundError:
                pass

        return None

    def locate(self) -> ConfigPath:
        """ Localise le fichier de configuration """

        path = ConfigPath(self.filename)

        # Extension non supportée
        if path.suffix and path.suffix not in self.supported_extensions:
            raise ExtensionNotSupportedError(path)

        # Si le chemin est absolu, on le retourne ou on déclenche une exception
        if path.is_absolute():
            return self._get_path_or_exception(path)

        new_path: Optional[ConfigPath] = None

        # S'il y a déjà une extension, on essaie de localiser le fichier relatif
        if path.suffix:
            new_path = self._locate_relative_path(path)

        else:
            # Sinon, on teste toutes les extensions supportées
            for ext in self.supported_extensions:
                new_path = self._locate_relative_path(path.with_suffix(ext))
                if new_path:
                    break

        if new_path:
            return new_path

        raise FileNotFoundError(f"Pas de fichier de configuration: {self.tested_paths}")
