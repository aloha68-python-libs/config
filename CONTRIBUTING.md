# Pour contribuer au projet

First, sorry about french speaking, it could evolve on future but for now I'm kind of lazy!

## Installation

Quelques commandes simples pour initialiser le projet

    git clone git@gitlab.com:aloha68-python-libs/config.git
    cd config
    poetry install

Après ça, on a accès aux sources avec toutes les dépendances installées.

## Tests

- Tests unitaires : `poetry run pytest --cov`
- Typage statique : `poetry run mypy aloha68_config`
- Formattage de code : `poetry run pylint aloha68_config`

## Publication

Liste des commandes à effectuer à chaque nouvelle version (je prépare un petit outil pour automatiser)

    poetry version {patch,minor,major}
    poetry build

Étant donné que je ne publie pas sur (pypi)[https://pypi.org], je n'utilise pas la commande `poetry publish`. Mais ça pourrait être aussi facile que ça !
