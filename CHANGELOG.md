# Changelog

Tous les changements notables apportés à ce projet seront documentés dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/) et ce projet adhère à la [gestion sémantique de version](https://semver.org/lang/fr/).

## [0.3.0] - 2021-10-29

### Nouveau
- La classe YamlConfig permet le déchiffrement de valeur. Cela peut être fait de manière interactive grâce au paramètre *interactive_decryption* ou alors par le biais d'une solution de keyring grâce aux paramètres *keyring_service_name* et *keyring_username*.
- Un test ajouté pour vérifier le déchiffrement en utilisant la méthode de keyring

### Modifié
- Prise en charge de python 3.7 et supérieur

## [0.2.1] - 2021-09-16

### Nouveau
- Amélioration des tests
- Intégration continue grâce aux pipelines gitlab

## [0.2.0] - 2021-07-10

### Nouveau
- Nouvelle classe ConfigLocator qui permet de rechercher un fichier de configuration à différents endroits
- Nouvelle classe YamlConfig qui utilise la classe YamlLoader déjà existante

### Modifié
- La classe Config passe en classe abstraite et doit être implémentée par chaque gestionnaire de config

[0.3.0]: https://gitlab.com/aloha68-python-libs/config/-/tags/0.3.0
[0.2.1]: https://gitlab.com/aloha68-python-libs/config/-/tags/0.2.1
[0.2.0]: https://gitlab.com/aloha68-python-libs/config/-/tags/0.2.0
